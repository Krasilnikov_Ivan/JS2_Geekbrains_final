(function($){
    var bookAction = '',
        page = 1;
    $(function(){
        var idBook = 0,
            idUser = 0,
            action = 'show';

        // Первоначальная инициализация плагинов
        $('.button-collapse').sideNav();
        getBooks('/books?action=show', 'col s4 book');
        $('.modal').modal();
        $('.datepicker').pickadate({
            selectMonths: false,
            selectYears: false,
            monthsFull: [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ],
            monthsShort: [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ],
            today: 'Сегодня',
            clear: '',
            close: 'Закрыть',
            format: 'yyyy-mm-dd',
            min: (new Date).getDate(),
            max: (new Date).getDate() + 30
        });

        // При клике на кнопку "подробнее" и на обложку книги
        $('body').on('click','.book .image a, .book .show-book', function (e) {
            e.preventDefault();
            action = 'show';
            page = 1;
            getBooks('/books?action=show&id_book=' + $(this).data('id'), 'container');
        });

        // При клике на название сайта
        $('#logo-container').on('click',function () {
            action = 'show';
            bookAction = '';
            page = 1;
            getBooks('/books?action=show', 'col s4 book');
        });

        // При клике "вернуть" в разделе "Мои книги"
        $('body').on('click','.remove-from-basket',function () {
            page = 1;
            $.getJSON('/books?id_book=' + $(this).data('id') + '&action=post', function (response) {
                if(response === true){
                    getBooks('/books?action=show&id_user=' + idUser, 'col s4 book');
                    Materialize.toast('Вы вернули книгу!', 1500);
                }else{
                    Materialize.toast('Произошла ошибка! Попробуйте позже', 1500);
                }
            })
        });

        // При клике на кнопку "взять"
        $('body').on('click','.book .booked a, .container .add-to-basket', function () {
            action = 'get';
            bookNumber = false;
            idBook = $(this).data('id');
            $('#booked').modal('open');
        });

        // При нажатии кнопки "взять книгу по номеру"
        $('body').on('click', '.get-book-by-number', function () {
            action = 'get';
            bookAction = '';
            $('#book-by-number').modal('open');
        });

        // При нажатии кнопки "вернуть книгу по номеру"
        $('body').on('click', '.post-book-by-number', function () {
            action = 'post';
            bookAction = '';
            $('#book-by-number').modal('open');
        });

        // При клике на кнопку "Мои книги"
        $('.get-my-book').on('click',function () {
            action = 'show';
            page = 1;
            bookAction = 'get-my-book';
            $('#booked').modal('open');
        });

        // При изменении поля "номер книги"
        $('#book-by-number input').on('keyup', function () {
            if($(this).val().length > 0){
                $('#book-by-number .modal-footer a').removeAttr('disabled');
            }else{
                $('#book-by-number .modal-footer a').attr('disabled','disabled');
            }
        });

        // При клике на кнопку продолжить при вводе номера книги
        $('#book-by-number .modal-footer a:not([disable])').on('click',function () {
            $.getJSON('/books?book_number=' + $('#book-by-number input').val() + '&action=' + action, function (response) {
                if(response !== null){
                    if(action === 'get') {
                        if (response === 'reserved') {
                            Materialize.toast('Эта книга уже забронирована! Возьмите другую внигу', 1500);
                        } else {
                            idBook = Number(response);
                            $('#book-by-number').modal('close');
                            $('#booked').modal('open');
                        }
                    }
                    if(action === 'post'){
                        if (response === 'freedom') {
                            Materialize.toast('Эта книга итак свободна!', 1500);
                        } else {
                            $.getJSON('/books?id_book=' + response + '&action=post', function (response) {
                                if(response === true){
                                    $('#book-by-number').modal('close');
                                    getBooks('/books?action=show', 'col s4 book');
                                    Materialize.toast('Вы вернули книгу!', 1500);
                                }else{
                                    Materialize.toast('Произошла ошибка! Попробуйте позже', 1500);
                                }
                            });
                        }
                    }
                }else{
                    Materialize.toast('Такая книга не найдена! Проверьте номер', 1500);
                }
            })
        });

        // При изменеии поля "номер читателя"
        $('#booked input').on('keyup', function () {
            if($(this).val().length > 0){
                $('#booked .modal-footer a').removeAttr('disabled');
            }else{
                $('#booked .modal-footer a').attr('disabled','disabled');
            }
        });

        // При клике на конопку "продолжить" при вводе номера читателя
        $('#booked .modal-footer a:not([disable])').on('click',function () {
            $.getJSON('/user?number=' + $('#booked input').val(), function (response) {
                if(response !== null){
                    idUser = response;
                    $('#booked').modal('close');
                    if(action === 'show'){
                        getBooks('/books?action=show&id_user=' + idUser, 'col s4 book');
                    }
                    if(action === 'get'){
                        $('#data-end').modal('open');
                        var $input = $('.datepicker').pickadate();
                        var picker = $input.pickadate('picker');
                        var inWeek = new Date();
                        inWeek.setDate(inWeek.getDate() + 7);
                        picker.set('select', inWeek);
                    }
                }else{
                    Materialize.toast('Такой читатель не найден! Проверьте номер', 1500);
                }
            })
        });

        // При клике на кнопку "подтвердить" при взятии вниги
        $('#data-end .modal-footer a').on('click',function () {
            bookedBook('/books?action=get&id_book=' + idBook + '&id_user=' + idUser + '&end=' + $('#data-end input').val());
        });

        // При клике на пагинацию
        $('body').on('click', '.pagination li:not(.active) a', function () {
           page = $(this).data('page');
            getBooks('/books?action=show', 'col s4 book');
        });

        $(window).on('popstate', function () {
            if(location.pathname === '/'){
                getBooks('/books', 'col s4 book');
            }
            if(location.pathname.indexOf('book')){
                getBooks(location.pathname, 'container');
            }
        })
    });

    // Бронирование книги
    function bookedBook(url) {
        $.getJSON(url, function (response) {
            if(response == 'reserved'){
                Materialize.toast('Эта книга уже забронирована. Выберите другую книгу', 1500);
            }else{
                if(response !== null) {
                    $('#data-end').modal('close');
                    Materialize.toast('Книга успешно забронирована', 1500);
                    getBooks('/books?action=show', 'col s4 book');
                }else{
                    Materialize.toast('Ошибка! Попробуйте позже', 1500);
                }
            }
        });
    }

    // Получение с сервера списка книг
    function getBooks(url, bookContainerClass) {
        $.getJSON(url + '&page=' + page, function (response) {
            $('.books').find('.row').empty();
            if(Array.isArray(response.data)){
                for(var i = 0; i < response.data.length; i++){
                    addBooksContainer(bookContainerClass, response.data[i]);
                }
            }
            if(response.count > 3){
                createPagination(response.count);
            }
        });
    }

    // Генерация контейнера для книг
    function addBooksContainer(bookContainerClass, book) {
        $('.books').find('.row').append(
            $('<div/>', {
                class: bookContainerClass
            }).append(bookJSONtoHTML(book))
        );
    }

    // Генерация из JSON'а книги полноценного HTML
    function bookJSONtoHTML(book) {
        var res = $('<div/>', {
            class: 'book-content card-panel hoverable'
        }).append(
            $('<div/>',{
                class: 'image'
            }).append(
                $('<a/>',{
                    href: '/books/' + book.id,
                    'data-id': book.id
                }).append(
                    $('<img />',{
                        src: book.image
                    })
                )
            )
        ).append(
            $('<div/>', {
                class: 'name',
                text: book.name
            })
        ).append(
            $('<div/>', {
                class: 'autor',
                text: book.autor
            }).prepend(
                $('<span/>',{
                    text: 'Автор: '
                })
            )
        ).append(
            $('<div/>', {
                class: 'publishing_house',
                text: book.publishing_house
            }).prepend(
                $('<span/>',{
                    text: 'Издательство: '
                })
            )
        ).append(
            $('<div/>', {
                class: 'annotation',
                text: book.annotation
            })
        );
        if(book.status === '1'){
            res.append(
                $('<div/>').append(
                    $('<a/>',{
                        text: 'Резерв до: ' + book.end
                    })
                )
            )
            if(bookAction === 'get-my-book'){
                res.append(
                    $('<div/>',{
                        class: 'col s6 returned'
                    }).append(
                        $('<a/>',{
                            class: 'remove-from-basket waves-effect waves-light',
                            text: 'Вернуть',
                            'data-id': book.id
                        })
                    )
                )
            }
        }else{
            res.append(
                $('<div/>',{
                    class: 'col s6 booked'
                }).append(
                    $('<a/>',{
                        class: 'add-to-basket waves-effect waves-light',
                        text: 'Взять',
                        'data-id': book.id
                    })
                )
            )
        }
        res.append(
            $('<div/>',{
                class: 'col s6'
            }).append(
                $('<a/>',{
                    class: 'show-book',
                    text: 'Подробнее',
                    href: '/books/' + book.id,
                    'data-id': book.id
                })
            )
        );

        return res;
    }

    // Генерация пагинации
    function createPagination(count) {
        var ul = $('<ul/>',{ class: 'pagination' });
        // стрелочка на предыдущую страницу
        if(page == 1){
            ul.append(
                $('<li/>', {
                    class: 'disabled'
                }).append(
                    $('<a/>',{
                        'data-page': i + 1
                    }).append(
                        $('<i/>',{
                            class: 'material-icons',
                            text: '<'
                        })
                    )
                )
            )
        }else{
            ul.append(
                $('<li/>', {
                    class: 'waves-effect'
                }).append(
                    $('<a/>',{
                        'data-page': i + 1
                    }).append(
                        $('<i/>',{
                            class: 'material-icons',
                            text: '<'
                        })
                    )
                )
            )
        }
        // Страницы с цифрами, с учётом активной
        for (var i = 0; i < count / 3; i++){
            if(i == page - 1){
                ul.append(
                    $('<li/>', {
                        class: 'active'
                    }).append(
                        $('<a/>',{
                            'data-page': i + 1,
                            text: i +1
                        })
                    )
                )
            }else{
                ul.append(
                    $('<li/>', {
                        class: 'waves-effect'
                    }).append(
                        $('<a/>',{
                            'data-page': i + 1,
                            text: i +1
                        })
                    )
                )
            }
        }
        // стрелочка на следующую страницу
        if(page == count / 3){
            ul.append(
                $('<li/>', {
                    class: 'disabled'
                }).append(
                    $('<a/>',{
                        'data-page': i + 1
                    }).append(
                        $('<i/>',{
                            class: 'material-icons',
                            text: '>'
                        })
                    )
                )
            )
        }else{
            ul.append(
                $('<li/>', {
                    class: 'waves-effect'
                }).append(
                    $('<a/>',{
                        'data-page': i + 1
                    }).append(
                        $('<i/>',{
                            class: 'material-icons',
                            text: '>'
                        })
                    )
                )
            )
        }
        $('.books').find('.row').append($('<hr/>'));
        $('.books').find('.row').append(ul);
    }
})(jQuery);