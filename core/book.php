<?php
require_once('db.php');

$sql = "SELECT book.id, book.name as name, book.annotation, book.image, autor.name as autor, publishing_house.name as publishing_house, booking.end, book.status
        FROM book
        LEFT JOIN autor ON (autor.id=book.id_autor)
        LEFT JOIN publishing_house ON (publishing_house.id=book.id_publishing_house)
        LEFT JOIN booking ON (booking.id_book=book.id)";

// Вернуть книгу по id
if(isset($_GET['id_book']) && $_GET['action'] === 'post'){
    try {
        $conn->beginTransaction();
        $sql = $conn->prepare("UPDATE booking SET `end`=:data_end");
        $sql->bindParam(':data_end', date('Y-m-d'),PDO::PARAM_STR);
        $res = $sql->execute();
        $conn->commit();
    }catch (PDOException $e){
        $e->getMessage();
        $conn->rollBack();
    }
    if($res) {
        $sql = $conn->prepare("UPDATE `book` SET status='0' WHERE `id`=?");
        $result = $sql->execute(array($_GET['id_book']));
    }else{
        $result = null;
    }
    print_r(json_encode($result));
    return true;
}

// Забронировать книгу по id
if(isset($_GET['id_book']) && $_GET['action'] === 'get'){
    try {
        $conn->beginTransaction();
        $sql = $conn->prepare("INSERT INTO booking (id_book, id_user, `begin`, `end`) 
                                         VALUES (:id_book, :id_user, :data_begin, :data_end) 
                                         ON DUPLICATE KEY UPDATE id_user=VALUES(id_user), `begin`=VALUES(`begin`), `end`=VALUES(`end`)");
        $sql->bindParam(':id_book', $_GET['id_book'], PDO::PARAM_INT);
        $sql->bindParam(':id_user', $_GET['id_user'], PDO::PARAM_INT);
        $sql->bindParam(':data_begin', date('Y-m-d'),PDO::PARAM_STR);
        $sql->bindParam(':data_end', $_GET['end'], PDO::PARAM_STR);
        $res = $sql->execute();
        $conn->commit();
    }catch (PDOException $e){
        print_r($e->getMessage());
        $conn->rollBack();
    }
    if($res) {
        $sql = $conn->prepare("UPDATE `book` SET status='1' WHERE `id`=?");
        $result = $sql->execute(array($_GET['id_book']));
    }else{
        $result = null;
    }
    print_r(json_encode($result));
    return true;
}

// Получить книгу по её id
if(isset($_GET['id_book']) && $_GET['action'] === 'show'){
    $sql .= 'WHERE book.id=' . $_GET['id_book'];
    $books = $conn->query($sql)->fetchAll();
    print_r(json_encode(convert_books($books)));
    return true;
}

// Получить все зарезервированные пользователем книги
if(isset($_GET['id_user']) && $_GET['action'] === 'show'){
    $sql .= ' WHERE booking.end>=CURDATE() AND book.status=1 AND booking.id_user=' . $_GET['id_user'];
    $books = $conn->query($sql)->fetchAll();
    $result = ($books) ? convert_books($books) : null;
    print_r(json_encode($result));
    return true;
}

// Получить все книги
if($_GET['action'] === 'show'){
    $books = $conn->query($sql)->fetchAll();
    print_r(json_encode(convert_books($books)));
    return true;
}

/* Проверяю свободна ли книга по номеру
* Если книги нет - вернёт null
* Если есть и зарезервирована - вернёт "reserved"
* Если есть и свободна - вернёт id книги
*/
if(isset($_GET['book_number'])){
    $sql .= ' WHERE book.number=' . $_GET['book_number'];
    $books = $conn->query($sql)->fetchAll();
    if($books) {
        if ($_GET['action'] === 'get'){
            if(is_reserved($books[0]['id'], $conn)) {
                $result = 'reserved';
            }else{
                $result = $books[0]['id'];
            }
        }
        if ($_GET['action'] === 'post'){
            if(!is_reserved($books[0]['id'], $conn)) {
                $result = 'freedom';
            }else{
                $result = $books[0]['id'];
            }
        }
    }else{
        $result = null;
    }
    print_r(json_encode($result));
}

// Проверяю по id не занята ли книга
function is_reserved($id_book, $conn)
{
    $sql2 = "SELECT status FROM book WHERE id=" . $id_book;
    $res = $conn->query($sql2)->fetchAll();
    if($res[0]['status'] == "1"){
        return true;
    }else{
        return false;
    }
}

// Превращаю результаты выборки в нормальный массив данных
function convert_books($books)
{
    $res = [];
    foreach ($books as $key=>$book) {
        if($key >= ($_GET['page'] - 1) * 3 && $key < $_GET['page'] * 3){
            $arr = array(
                'id' => $book['id'],
                'name' => $book['name'],
                'publishing_house' => $book['publishing_house'],
                'image' => $book['image'],
                'annotation' => mb_strimwidth($book['annotation'], 0, 200) . '...',
                'autor' => $book['autor'],
                'end' => $book['end'],
                'status' => $book['status'],
            );
            array_push($res, $arr);
        }
    }
    $result = [];
    $result['count'] = count($books);
    $result['data'] = $res;
    return $result;
}